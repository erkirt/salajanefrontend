
function greet(){
  let test = document.getElementById("test");

// This handler will be executed only once when the cursor
// moves over the unordered list
  test.addEventListener("mouseenter", function( event ) {
    // highlight the mouseenter target
    event.target.style.color = "yellow";
  }, false);
}
