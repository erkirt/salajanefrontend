import { Component, OnInit } from '@angular/core';
import {Contact} from "../contact/contact";
import {ContactService} from "../contact/contact.service";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {window} from "rxjs";

@Component({
  selector: 'app-contact-search',
  templateUrl: './contact-search.component.html',
  styleUrls: ['./contact-search.component.css']
})
export class ContactSearchComponent implements OnInit {

  public contacts: Contact[] = [];
  public searchTerm : string = "";

  constructor(private contactService : ContactService,
              private route: ActivatedRoute,
              private router : Router) { }

  ngOnInit(): void {
    this.searchTerm = this.route.snapshot.params['searchTerm'];
    if (this.searchTerm){
      this.contactService.getSearchedContacts(this.searchTerm).subscribe(
        (response: Contact[]) => {
          this.contacts = response;
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );}
  }

  updateContact(id: number | undefined){
    this.router.navigate(['contact-by-id', id  ])
  }

  detailedContact(id: number | undefined){
    this.router.navigate(['details', id])
  }


}
