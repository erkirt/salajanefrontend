import { Component, OnInit } from '@angular/core';
import {Contact} from "../contact/contact";
import {ActivatedRoute, Router} from "@angular/router";
import {ContactService} from "../contact/contact.service";

@Component({
  selector: 'app-contact-by-id',
  templateUrl: './contact-by-id.component.html',
  styleUrls: ['./contact-by-id.component.css']
})
export class ContactByIdComponent implements OnInit {

  id: number | undefined;
  contact: Contact =  new Contact();

  constructor(private contactService: ContactService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.contactService.getContactById(this.id).subscribe(data => {
      console.log(data)
      // @ts-ignore
      this.contact = data;
    }, error => console.log(error))
  }

  updateContact(){
    this.contactService.UpdateContactById(this.contact,this.id).subscribe(data =>{
        this.goToContactList();
      }
      , error => console.log(error))
  }

  onSubmit(){
    this.updateContact();
  }

  goToContactList(){
    this.router.navigate(['/contact-list']);
  }


}
