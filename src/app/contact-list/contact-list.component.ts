import { Component, OnInit } from '@angular/core';
import {Contact} from "../contact/contact";
import {ContactService} from "../contact/contact.service";
import {Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {

  public changeText = {};
  public contacts!: Contact[];

  constructor(private contactService : ContactService,
              private router : Router,
              ) { }

  ngOnInit(): void {
    this.getContacts();
    this.changeText = false;
  }

  public getContacts() : void{
    this.contactService.getContacts().subscribe(
      (response: Contact[]) => {
        this.contacts = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  updateContact(id: number | undefined){
    this.router.navigate(['contact-by-id', id  ])
  }

  detailedContact(id: number | undefined){
    this.router.navigate(['details', id])
  }
}
