import {Component, OnInit} from '@angular/core';
import {Contact} from "./contact/contact";
import {ContactService} from "./contact/contact.service";
import {HttpErrorResponse} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'SalajaneFrontend';

  searchTerm!: String;


  public  contact : Contact[] = [];



  constructor(private contactService : ContactService,
              private route: ActivatedRoute,
              private router : Router) {}

  ngOnInit() {
    this.getContacts();
  }

  public getContacts() : void{
    this.contactService.getContacts().subscribe(
      (response: Contact[]) => {
        this.contact = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  search():void{
    if(this.searchTerm)
      this.router.navigateByUrl('/search/' + this.searchTerm).then(() => {
        window.location.reload();
      });
  }

}
