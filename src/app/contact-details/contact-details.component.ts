import { Component, OnInit } from '@angular/core';
import {Contact} from "../contact/contact";
import {ContactService} from "../contact/contact.service";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.css']
})
export class ContactDetailsComponent implements OnInit {

  id: number | undefined;
  public changeText = {};
  contacts!: Contact[];
  constructor(private contactService: ContactService,
              private route: ActivatedRoute,) {
  }

  ngOnInit(): void {
    this.getContacts()
    this.changeText = false;
  }
  public getContacts() : void{
    this.id = this.route.snapshot.params['id'];
    this.contactService.getContactDetailsById(this.id).subscribe(
      (response: Contact[]) => {
        this.contacts = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
}
