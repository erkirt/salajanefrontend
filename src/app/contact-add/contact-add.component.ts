import { Component, OnInit } from '@angular/core';
import {Contact} from "../contact/contact";
import {ContactService} from "../contact/contact.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.css']
})
export class ContactAddComponent implements OnInit {

  id: number | undefined;
  contact: Contact =  new Contact();

  constructor(private contactService: ContactService,
              private router: Router) { }

  ngOnInit(): void {
  }

  saveContact(){
    this.contactService.addContacts(this.contact).subscribe( (data: any) =>{
        console.log(data);
        this.goToContactList();
      },
      (error: any) => console.log(error));
  }

  goToContactList(){
    this.router.navigate(['/contact-list']);
  }

  onSubmit(){
    console.log(this.contact);
    this.saveContact();
  }
}
