export class Contact{
  id: number | undefined;
  firstName: string | undefined;
  lastName: string | undefined;
  codeName: string | undefined;
  phoneNumber: string | undefined;
}
