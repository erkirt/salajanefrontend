import { Contact } from './contact';
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable(
  {
    providedIn : "root"
  }
)
export class ContactService{
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {}

  public getContacts(): Observable<Contact[]>{
    return this.http.get<Contact[]>(this.apiServerUrl+'/contact/all')
  }
  public addContacts(contact : Contact): Observable<Contact[]>{
    return this.http.post<Contact[]>(this.apiServerUrl+'/contact/add', contact)
  }
  public getContactById(id: number | undefined): Observable<Contact[]>{
    return this.http.get<Contact[]>(this.apiServerUrl+'/contact/' + id)
  }
  public getContactDetailsById(id: number | undefined): Observable<Contact[]>{
    return this.http.get<Contact[]>(this.apiServerUrl+'/contact/details/' + id)
  }
  public UpdateContactById(contact: Contact, id: number | undefined): Observable<Contact[]>{
    return this.http.put<Contact[]>(this.apiServerUrl+'/contact/'+id, contact)
  }
  public DeleteContactById(id: number | undefined): Observable<any>{
    return this.http.delete<Contact>(this.apiServerUrl+'/contact/' + id)
  }
  public getSearchedContacts(searchTerm: string): Observable<Contact[]>{
    return this.http.get<Contact[]>(this.apiServerUrl+'/contact/search/'+searchTerm)
  }
}
