import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {ContactListComponent} from "./contact-list/contact-list.component";
import {ContactAddComponent} from "./contact-add/contact-add.component";
import {ContactByIdComponent} from "./contact-by-id/contact-by-id.component";
import {ContactSearchComponent} from "./contact-search/contact-search.component";
import {ContactDetailsComponent} from "./contact-details/contact-details.component";

const routes : Routes = [
  {path: "contact-list", component: ContactListComponent},
  {path: "contact-form", component: ContactAddComponent},
  {path: "contact-by-id/:id", component: ContactByIdComponent},
  {path: "search/:searchTerm", component: ContactSearchComponent},
  {path: "details/:id", component: ContactDetailsComponent},
  {path: '', redirectTo: 'contact-list', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule{}
