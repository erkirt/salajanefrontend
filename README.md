# SalajaneFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Codenames

salajane kood-nimi (codename) kontaktide nimekirjas kuvatud ei ole. Selle nägemiseks, kas vajutage update, kus on see kirjas. 

Teine võimalus on vajutada nuppu nimega Details. Hõljudes nimekaardi peale, on selle allosas kirjas Codename. 
Codename nägemiseks peate hiirega klõpsama teksti peale "Click and Hold"


